package com.yurakhodanitcky.blog.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

    private String resourceName;
    private String fieldName;
    private Object fildValue;

    public ResourceNotFoundException(String resourceName, String fieldName, Object fildValue){
        super(String.format("%s not found with %s: '%s", resourceName, fieldName, fildValue));
        this.resourceName = resourceName;
        this.fieldName = fieldName;
        this.fildValue = fildValue;
    }
}
