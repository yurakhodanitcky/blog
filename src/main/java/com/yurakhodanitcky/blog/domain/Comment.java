package com.yurakhodanitcky.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Comment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    private String comment;

    @ManyToOne
    @JsonIgnore
    private Post post;

    @ManyToOne
    private User user;

    public Comment() {
    }

    public Long getId() {
        return this.id;
    }

    public @NotNull String getComment() {
        return this.comment;
    }

    public Post getPost() {
        return this.post;
    }

    public User getUser() {
        return this.user;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setComment(@NotNull String comment) {
        this.comment = comment;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Comment comment = (Comment) o;
        if (comment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), comment.getId());
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    public String toString() {
        return "Comment(id=" +
                this.getId() + ", comment=" +
                this.getComment() + ", post=" +
                this.getPost() + ", user=" +
                this.getUser() + ")";
    }
}
