package com.yurakhodanitcky.blog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Entity(name = "roles")
public class Role implements Serializable {

    @NotNull
    @Size(max = 50)
    @Id
    @Column(length = 50, unique = true)
    private String name;

    public Role() {
    }

    public @NotNull @Size(max = 50) String getName() {
        return this.name;
    }

    public void setName(@NotNull @Size(max = 50) String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Role role = (Role) o;
        if (role.getName() == null || getName() == null) {
            return false;
        }
        return Objects.equals(getName(), role.getName());
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(getName());
    }

    public String toString() {
        return "Role(name=" + this.getName() + ")";
    }
}
