package com.yurakhodanitcky.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "blog_user")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 3, max = 50)
    @Column(length = 50, unique = true, nullable = false)
    private String username;

    @NotNull
    @Size(max = 60)
    @JsonIgnore
    private String password;

    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 100)
    @Column(length = 100, unique = true)
    @JsonIgnore
    private String email;


    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Post> post = new HashSet<>();


    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Comment> comment = new HashSet<>();


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "name"))
    private Set<Role> roles = new HashSet<>();

    public User (String username, String password, String email){
        this.username = username;
        this.password = password;
        this.email = email;
    }
    public User (){}

    public Long getId() {
        return this.id;
    }

    public @NotNull @Size(min = 3, max = 50) String getUsername() {
        return this.username;
    }

    public @NotNull @Size(max = 100) String getPassword() {
        return this.password;
    }

    public @Size(max = 50) String getFirstName() {
        return this.firstName;
    }

    public @Size(max = 50) String getLastName() {
        return this.lastName;
    }

    public @Email @Size(min = 5, max = 100) String getEmail() {
        return this.email;
    }

    public Set<Post> getPost() {
        return this.post;
    }

    public Set<Comment> getComment() {
        return this.comment;
    }

    public Set<Role> getRoles() {
        return this.roles;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(@NotNull @Size(min = 3, max = 50) String username) {
        this.username = username;
    }

    public void setPassword(@NotNull @Size(max = 100) String password) {
        this.password = password;
    }

    public void setFirstName(@Size(max = 50) String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(@Size(max = 50) String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(@Email @Size(min = 5, max = 100) String email) {
        this.email = email;
    }

    public void setPost(Set<Post> post) {
        this.post = post;
    }

    public void setComment(Set<Comment> comment) {
        this.comment = comment;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        if (user.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), user.getId());
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    public String toString() {
        return "User(id=" +
                this.getId() + ", username=" +
                this.getUsername() + ", password=" +
                this.getPassword() + ", firstName=" +
                this.getFirstName() + ", lastName=" +
                this.getLastName() + ", email=" +
                this.getEmail() +/* ", post=" +
                this.getPost() + */", comment=" +
                this.getComment() + ", roles=" +
                this.getRoles() + ")";
    }
}
