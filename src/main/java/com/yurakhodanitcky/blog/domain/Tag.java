package com.yurakhodanitcky.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Tag implements Serializable {

    @NotNull
    @Size(max = 50)
    @Id
    @Column(length = 50, unique = true)
    private String name;

    public Tag(@NotNull @Size(max = 50) String name) {
        this.name = name;
    }
    public Tag(){}

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "tags")
    @JsonIgnore
    private Set<Post> posts = new HashSet<>();

    public @NotNull @Size(max = 50) String getName() {
        return this.name;
    }

    public Set<Post> getPosts() {
        return this.posts;
    }

    public void setName(@NotNull @Size(max = 50) String name) {
        this.name = name;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tag tag = (Tag) o;
        if (tag.getName() == null || getName() == null) {
            return false;
        }
        return Objects.equals(getName(), tag.getName());
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(getName());
    }

    public String toString() {
        return "Tag(name=" +
                this.getName()  +/*+ ", posts=" +
                this.getPosts() + */")";
    }
}
