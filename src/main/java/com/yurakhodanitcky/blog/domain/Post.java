package com.yurakhodanitcky.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
@Entity
public class Post implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    private String title;

    @NotNull
    @Column(name = "post_text")
    private String postText;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinTable(name = "post_tag",
            joinColumns = @JoinColumn(name = "post_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "name", referencedColumnName = "name"))
    @JsonIgnore
    private Set<Tag> tags = new HashSet<>();

    @OneToMany(mappedBy = "post", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Comment> comment = new HashSet<>();

    @ManyToOne
    @JsonIgnore
    private User user;

    public Post() {
    }

    public Long getId() {
        return this.id;
    }

    public @NotNull String getTitle() {
        return this.title;
    }

    public @NotNull String getPostText() {
        return this.postText;
    }

    public LocalDateTime getCreatedAt() {
        return this.createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public Set<Tag> getTags() {
        return this.tags;
    }

    public Set<Comment> getComment() {
        return this.comment;
    }

    public User getUser() {
        return this.user;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(@NotNull String title) {
        this.title = title;
    }

    public void setPostText(@NotNull String postText) {
        this.postText = postText;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public void setComment(Set<Comment> comment) {
        this.comment = comment;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Post post = (Post) o;
        if (post.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), post.getId());
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    public String toString() {
        return "Post(id=" +
                this.getId() + ", title=" +
                this.getTitle() + ", postText=" +
                this.getPostText() + ", createdAt=" +
                this.getCreatedAt() + ", updatedAt=" +
                this.getUpdatedAt() + /*", tags=" +
                this.getTags() + */", comment=" +
                this.getComment() + ", user=" +
                this.getUser() + ")";
    }
}
