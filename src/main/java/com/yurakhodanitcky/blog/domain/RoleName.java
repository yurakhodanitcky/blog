package com.yurakhodanitcky.blog.domain;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
