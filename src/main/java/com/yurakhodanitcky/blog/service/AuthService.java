package com.yurakhodanitcky.blog.service;

import com.yurakhodanitcky.blog.domain.Role;
import com.yurakhodanitcky.blog.domain.RoleName;
import com.yurakhodanitcky.blog.domain.User;
import com.yurakhodanitcky.blog.exception.BadRequestException;
import com.yurakhodanitcky.blog.payload.ApiResponse;
import com.yurakhodanitcky.blog.payload.JwtAuthenticationResponse;
import com.yurakhodanitcky.blog.repository.RoleRepository;
import com.yurakhodanitcky.blog.repository.UserRepository;
import com.yurakhodanitcky.blog.security.JwtTokenProvider;
import com.yurakhodanitcky.blog.service.dto.user.SignupRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class AuthService {

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider tokenProvider;

    public AuthService(AuthenticationManager authenticationManager, UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder, JwtTokenProvider tokenProvider) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
    }

    public JwtAuthenticationResponse authenticateUser(SignupRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return new JwtAuthenticationResponse(jwt);
    }

    public User registerUser(SignupRequest signUpRequest){
        if(userRepository.existsByEmailOrUsername(signUpRequest.getEmail(), signUpRequest.getUsername())) {
            throw new BadRequestException("Username or email is already taken!");
        }
        // Creating user's account
        User user = new User(signUpRequest.getUsername(), passwordEncoder.encode(signUpRequest.getPassword()), signUpRequest.getEmail());
        Role role = roleRepository.getOne(RoleName.ROLE_USER.toString());
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);
        User result = userRepository.save(user);
        return user;
    }
}
