package com.yurakhodanitcky.blog.service.mapper;

import com.yurakhodanitcky.blog.domain.Post;
import com.yurakhodanitcky.blog.service.dto.post.CreatePostRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PostRequestMapper {
    CreatePostRequest toDto(Post post);
    Post toEntity (CreatePostRequest createPostRequest);
}
