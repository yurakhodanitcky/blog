package com.yurakhodanitcky.blog.service.mapper;

import com.yurakhodanitcky.blog.domain.Post;
import com.yurakhodanitcky.blog.service.dto.post.PostDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CommentMapper.class})
public interface PostMapper {

    @Mappings({@Mapping(target = "comments", source = "comment"),
            @Mapping(target = "userName", source = "user.username"),
            @Mapping(target = "tags")})
    PostDto toDto(Post post);

    List<PostDto> toDto(List<Post> posts);

    default Page<PostDto> toDto(Page<Post> page) {
        return page.map(this::toDto);
    }

    @Mappings({@Mapping(target = "comment", source = "comments"),
            @Mapping(target = "tags")})
    Post toEntity(PostDto postDto);

    List<Post> toEntity(List<PostDto> postDtos);

    default Page<Post> toEntity(Page<PostDto> page) {
        return page.map(this::toEntity);
    }
}
