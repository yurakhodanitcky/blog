package com.yurakhodanitcky.blog.service.mapper;

import com.yurakhodanitcky.blog.domain.Post;
import com.yurakhodanitcky.blog.service.dto.post.PostPageableDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")

public interface PostPageableMapper {

    @Mappings({
            @Mapping(target = "title", source = "title"),
            @Mapping(source = "postText", target = "postText")
    })
    PostPageableDto toDto(Post post);
    List<PostPageableDto> toDto(List<Post> posts);
    default Page<PostPageableDto> toDto(Page<Post> page) {
        return page.map(this::toDto);
    }


    Post toEntity(PostPageableDto postDto);

    List<Post> toEntity(List<PostPageableDto> postDtos);

    default Page<Post> toEntity(Page<PostPageableDto> page) {
        return page.map(this::toEntity);
    }
}