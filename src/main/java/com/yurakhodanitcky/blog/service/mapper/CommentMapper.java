package com.yurakhodanitcky.blog.service.mapper;

import com.yurakhodanitcky.blog.domain.Comment;
import com.yurakhodanitcky.blog.service.dto.comment.CommentDto;
import com.yurakhodanitcky.blog.service.dto.comment.CreateCommentRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CommentMapper {

    @Mapping(target = "userName", source = "user.username")
    CommentDto toDto (Comment comment);
    List<CommentDto> toDto (List<Comment> comments);

    @Mapping(target = "user.username", source = "userName")
    Comment toEntity (CommentDto commentDto);
    Comment fromCommentRequest(CreateCommentRequest createCommentRequest);
}
