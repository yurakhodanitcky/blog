package com.yurakhodanitcky.blog.service;

import com.yurakhodanitcky.blog.domain.Role;
import com.yurakhodanitcky.blog.domain.RoleName;
import com.yurakhodanitcky.blog.domain.User;
import com.yurakhodanitcky.blog.exception.ResourceNotFoundException;
import com.yurakhodanitcky.blog.service.dto.user.SignupRequest;
import com.yurakhodanitcky.blog.service.dto.user.UpdateUser;
import com.yurakhodanitcky.blog.repository.RoleRepository;
import com.yurakhodanitcky.blog.repository.UserRepository;
import com.yurakhodanitcky.blog.security.UserPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class UserService {


    private  final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    public UserService(RoleRepository roleRepository, PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    public User save(SignupRequest signupRequest) {
        User user = new User(signupRequest.getUsername(), passwordEncoder.encode(signupRequest.getPassword()), signupRequest.getEmail());
        Role role = roleRepository.getOne(RoleName.ROLE_USER.toString());
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);
        return userRepository.save(user);
    }

    public User update(UserPrincipal currentUser, UpdateUser updateUser) {
        User user = findById(updateUser.getId());
        if(currentUser.getUsername().equals(user.getUsername())) {
            user.setFirstName(updateUser.getFirstName());
            user.setLastName(updateUser.getLastName());
        }
        return userRepository.save(user);
    }

    @Transactional(readOnly = true)
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Transactional(readOnly = true)
    public User findById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
        return user;
    }

    public void delete(Long id) {
        User user = findById(id);
        userRepository.delete(user);
    }

    @Transactional(readOnly = true)
    public boolean existsByEmailOrUsername(String email, String username) {
        return userRepository.existsByEmailOrUsername(email, username);
    }
}
