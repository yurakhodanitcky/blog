package com.yurakhodanitcky.blog.service;

import com.yurakhodanitcky.blog.domain.Comment;
import com.yurakhodanitcky.blog.domain.Post;
import com.yurakhodanitcky.blog.exception.ForbiddenException;
import com.yurakhodanitcky.blog.exception.ResourceNotFoundException;
import com.yurakhodanitcky.blog.repository.CommentRepository;
import com.yurakhodanitcky.blog.repository.PostRepository;
import com.yurakhodanitcky.blog.repository.UserRepository;
import com.yurakhodanitcky.blog.security.UserPrincipal;
import com.yurakhodanitcky.blog.service.dto.comment.CommentDto;
import com.yurakhodanitcky.blog.service.dto.comment.CreateCommentRequest;
import com.yurakhodanitcky.blog.service.mapper.CommentMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CommentService {
    private final PostRepository postRepository;
    private final CommentMapper commentMapper;
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;

    public CommentService(PostRepository postRepository, CommentMapper commentMapper, UserRepository userRepository, CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.commentMapper = commentMapper;
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
    }

    public CommentDto save(UserPrincipal currentUser, CreateCommentRequest createCommentRequest, Long id) {
        Comment comment = commentMapper.fromCommentRequest(createCommentRequest);
        Post post = postRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Post", "id", id));
        comment.setPost(post);
        comment.setUser(userRepository.findByUsername(currentUser.getUsername()));
        return commentMapper.toDto(commentRepository.save(comment));
    }

    public CommentDto update(UserPrincipal currentUser, CommentDto commentDto, Long id){
        Comment comment = findOneById(commentDto.getId());
        comment.setComment(commentDto.getComment());
        Post post = postRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Post", "id", id));
        comment.setPost(post);
        comment.setUser(userRepository.findByUsername(currentUser.getUsername()));
        return commentMapper.toDto(commentRepository.save(comment));
    }

    @Transactional(readOnly = true)
    public List<CommentDto> findAll() {
        return commentMapper.toDto(commentRepository.findAll());
    }

    @Transactional(readOnly = true)
    public CommentDto findOne(Long id) {
        Comment comment = findOneById(id);
        return commentMapper.toDto(comment);
    }

    public void delete(Long id, UserPrincipal currentUser) {
        Comment comment = commentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Comment", "id", id));
        if(comment.getUser().getUsername().equals(currentUser.getUsername())) {
            commentRepository.delete(comment);
        }
        else throw new ForbiddenException("You can delete only your own comments!");
    }

    private Comment findOneById(Long id){
       return commentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Comment", "id", id));
    }

}
