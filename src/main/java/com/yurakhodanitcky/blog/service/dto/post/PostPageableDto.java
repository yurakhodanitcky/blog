package com.yurakhodanitcky.blog.service.dto.post;

import lombok.Data;

@Data
public class PostPageableDto {
    private String title;
    private String postText;
}
