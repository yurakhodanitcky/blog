package com.yurakhodanitcky.blog.service.dto.tag;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class TagDto implements Serializable {

    @Getter
    @Setter
    private String name;

    public TagDto(String s) {
        this.name = s;
    }
    public TagDto (){}
}
