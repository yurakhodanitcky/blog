package com.yurakhodanitcky.blog.service.dto.user;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
public class SignupRequest {
    private String username;
    @Size(min = 4, max = 8, message = "Password size must be between 4 and 8")
    private String password;
    @Email
    private String email;

    public SignupRequest() {
    }

    public SignupRequest(String username, String password, String email) {

        this.username = username;
        this.password = password;
        this.email = email;
    }
}
