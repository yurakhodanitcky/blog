package com.yurakhodanitcky.blog.service.dto.comment;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

public class CreateCommentRequest {
    @Size(max = 500, message = "Comment must be not longer 500")
    @Getter
    @Setter
    private String comment;
}
