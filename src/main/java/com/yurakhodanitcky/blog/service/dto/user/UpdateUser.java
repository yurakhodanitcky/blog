package com.yurakhodanitcky.blog.service.dto.user;

import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
public class UpdateUser {
    @NotNull
    private Long id;
    private String firstName;
    private String lastName;
}
