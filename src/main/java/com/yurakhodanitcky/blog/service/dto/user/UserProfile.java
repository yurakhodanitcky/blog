package com.yurakhodanitcky.blog.service.dto.user;

import com.yurakhodanitcky.blog.domain.User;
import lombok.Data;

@Data
public class UserProfile {
    private Long id;
    private String username;
    private String firstName;
    private String lastName;

    public UserProfile(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
    }

}
