package com.yurakhodanitcky.blog.service.dto.comment;

import lombok.Data;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class CommentDto implements Serializable {

    private Long id;

    @Size(max = 500, message = "Comment must be not longer 500")
    private String comment;

    private String userName;
}
