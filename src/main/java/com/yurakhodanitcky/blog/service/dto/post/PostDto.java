package com.yurakhodanitcky.blog.service.dto.post;

import com.yurakhodanitcky.blog.service.dto.tag.TagDto;
import com.yurakhodanitcky.blog.service.dto.comment.CommentDto;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class PostDto implements Serializable {

    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter
    @Size(max = 150, message = "Post title must be not longer 150")
    private String title;
    @Getter
    @Setter
    @Size(max = 5555, message = "Post text must be not longer 5555")
    private String postText;
    @Getter
    @Setter
    private String userName;
    @Getter
    @Setter
    private LocalDateTime createdAt;
    @Getter
    @Setter
    private List<TagDto> tags;
    @Getter
    @Setter
    private List<CommentDto> comments;
}
