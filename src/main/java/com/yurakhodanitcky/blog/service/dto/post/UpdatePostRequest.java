package com.yurakhodanitcky.blog.service.dto.post;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

public class UpdatePostRequest {
    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter
    @Size(max = 150, message = "Post text must be not longer 150")
    private String title;
    @Getter
    @Setter
    @Size(max = 5555, message = "Post text must be not longer 5555")
    private String postText;
    @Getter
    @Setter
    private String tagString;
    @Getter
    @Setter
    private String userName;
}
