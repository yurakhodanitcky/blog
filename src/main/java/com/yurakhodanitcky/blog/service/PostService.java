package com.yurakhodanitcky.blog.service;

import com.yurakhodanitcky.blog.domain.Post;
import com.yurakhodanitcky.blog.domain.Tag;
import com.yurakhodanitcky.blog.domain.User;
import com.yurakhodanitcky.blog.exception.ForbiddenException;
import com.yurakhodanitcky.blog.exception.ResourceNotFoundException;
import com.yurakhodanitcky.blog.repository.PostRepository;
import com.yurakhodanitcky.blog.repository.TagRepository;
import com.yurakhodanitcky.blog.repository.UserRepository;
import com.yurakhodanitcky.blog.security.UserPrincipal;
import com.yurakhodanitcky.blog.service.dto.post.CreatePostRequest;
import com.yurakhodanitcky.blog.service.dto.post.PostDto;
import com.yurakhodanitcky.blog.service.dto.post.PostPageableDto;
import com.yurakhodanitcky.blog.service.dto.post.UpdatePostRequest;
import com.yurakhodanitcky.blog.service.mapper.PostRequestMapper;
import com.yurakhodanitcky.blog.service.mapper.PostMapper;
import com.yurakhodanitcky.blog.service.mapper.PostPageableMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class PostService {

    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final PostMapper postMapper;
    private final TagRepository tagRepository;
    private final PostPageableMapper postPageableMapper;
    private final PostRequestMapper postRequestMapper;

    public PostService(UserRepository userRepository, PostRepository postRepository, PostMapper postMapper, TagRepository tagRepository, PostPageableMapper postPageableMapper, PostRequestMapper postRequestMapper) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.postMapper = postMapper;
        this.tagRepository = tagRepository;
        this.postPageableMapper = postPageableMapper;
        this.postRequestMapper = postRequestMapper;
    }

    public PostDto save(UserPrincipal currentUser, CreatePostRequest createPostRequest) {
        Post post = postRequestMapper.toEntity(createPostRequest);
        post.setUser(userRepository.findByUsername(currentUser.getUsername()));
        post.setTags(setTagsFromString(createPostRequest.getTagString()));
        tagRepository.saveAll(post.getTags());
        post.setCreatedAt(LocalDateTime.now());
        return postMapper.toDto(postRepository.save(post));
    }

    @Transactional(readOnly = true)
    public List<PostDto> findAll() {
        return postMapper.toDto(postRepository.findAllByOrderByCreatedAtDesc());
    }

    @Transactional(readOnly = true)
    public PostDto findOne(Long id) {
        Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));
        return postMapper.toDto(post);
    }

    public void delete(Long id, UserPrincipal currentUser) {
        Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));
        if (currentUser.getUsername().equals(post.getUser().getUsername())) {
            postRepository.delete(post);
            for(Tag t: post.getTags()){
                if(t.getPosts().size() == 1)
                    tagRepository.delete(t);
            }
        }
        else throw new ForbiddenException("You can delete only your own posts!");
    }

    @Transactional(readOnly = true)
    public List<PostDto> findAllByTag(String tag) {
        Tag t = tagRepository.getOne(tag);
        return postMapper.toDto(postRepository.findAllByTags(t));
    }

    @Transactional(readOnly = true)
    public List<PostDto> findAllByUser(String username) {
        User user = userRepository.findByUsername(username);
        List<Post> posts = postRepository.findAllByUser(user);
        return postMapper.toDto(posts);
    }

    @Transactional(readOnly = true)
    public Page<PostPageableDto> findPage(int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Order.desc("created_at")));
        Page<Post> posts = postRepository.findPageable(pageable);
        return postPageableMapper.toDto(posts);
    }

    private Set<Tag> setTagsFromString(String tag) {
        Set<Tag> tags = new HashSet<>();
        if (tag != null) {
            String[] tagString = tag.split("\\s*(,|\\s)\\s*");
            for (String s : tagString) {
                Tag t = new Tag(s);
                tags.add(t);
            }
        }
        return tags;
    }

    public PostDto update(@Valid UpdatePostRequest updatePostRequest) {
        Post post = postRepository.findById(updatePostRequest.getId()).orElseThrow(() -> new ResourceNotFoundException("Post", "id", updatePostRequest.getId()));
        post.setTitle(updatePostRequest.getTitle());
        post.setPostText(updatePostRequest.getPostText());
        Set<Tag> tags = post.getTags();
        tags.addAll(setTagsFromString(updatePostRequest.getTagString()));
        post.setTags(tags);
        post.setUser(userRepository.findByUsername(updatePostRequest.getUserName()));
        tagRepository.saveAll(post.getTags());
        post.setUpdatedAt(LocalDateTime.now());
        return postMapper.toDto(postRepository.save(post));
    }
}
