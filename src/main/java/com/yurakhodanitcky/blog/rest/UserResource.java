package com.yurakhodanitcky.blog.rest;

import com.yurakhodanitcky.blog.domain.User;
import com.yurakhodanitcky.blog.exception.BadRequestException;
import com.yurakhodanitcky.blog.payload.ApiResponse;
import com.yurakhodanitcky.blog.service.dto.user.SignupRequest;
import com.yurakhodanitcky.blog.service.dto.user.UpdateUser;
import com.yurakhodanitcky.blog.service.dto.user.UserProfile;
import com.yurakhodanitcky.blog.security.UserPrincipal;
import com.yurakhodanitcky.blog.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserResource {

    private final UserService userService;

    public UserResource(UserService userService) {
        this.userService = userService;
    }
    @PostMapping("/users")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> createUser (@Valid @RequestBody SignupRequest signupRequest, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            throw new BadRequestException(bindingResult.getFieldError().getDefaultMessage());
        }
        if (!userService.existsByEmailOrUsername(signupRequest.getEmail(), signupRequest.getUsername())) {
            User user = userService.save(signupRequest);
            UserProfile userProfile = new UserProfile(user);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getId()).toUri();
            return ResponseEntity.created(location).body(userProfile);
        }
        return new ResponseEntity(new ApiResponse(false, "Username or email is already taken!"), HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/users")
    public ResponseEntity<?> updateUser(@ApiIgnore @AuthenticationPrincipal UserPrincipal currentUser, @RequestBody UpdateUser updateUser) {
        User user = userService.findById(updateUser.getId());
        if(updateUser.getId() != null&&user.getUsername().equals(currentUser.getUsername())) {
            user = userService.update(currentUser, updateUser);
            UserProfile userProfile = new UserProfile(user);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getId()).toUri();
            return ResponseEntity.created(location).body(userProfile);
        }
        return new ResponseEntity(new ApiResponse(false, "You can edit only your own profile!"), HttpStatus.FORBIDDEN);
    }

    @GetMapping("/users")
    @Secured("ROLE_ADMIN")
    public List<User> getAllUsers (){
       return userService.findAll();
    }

    @GetMapping("/users/{id}")
    @Secured("ROLE_ADMIN")
    public User getUser (@PathVariable Long id) {
        return userService.findById(id);
    }

    @DeleteMapping("/users/{id}")
    @Secured("ROLE_ADMIN")
    public void deleteUser (@PathVariable Long id){
        userService.delete(id);
    }
}
