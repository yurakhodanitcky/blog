package com.yurakhodanitcky.blog.rest;

import com.yurakhodanitcky.blog.exception.BadRequestException;
import com.yurakhodanitcky.blog.payload.ApiResponse;
import com.yurakhodanitcky.blog.security.UserPrincipal;
import com.yurakhodanitcky.blog.service.CommentService;
import com.yurakhodanitcky.blog.service.dto.comment.CommentDto;
import com.yurakhodanitcky.blog.service.dto.comment.CreateCommentRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CommentResource {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private final CommentService commentService;

    public CommentResource(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/comments/{post_id}")
    public ResponseEntity<?> createComment(@ApiIgnore @AuthenticationPrincipal UserPrincipal currentUser, @Valid @RequestBody CreateCommentRequest createCommentRequest, @PathVariable Long post_id, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            throw new BadRequestException(bindingResult.getFieldError().getDefaultMessage());
        }
        CommentDto result = commentService.save(currentUser, createCommentRequest, post_id);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).body(result);
    }

    @PutMapping("/comments/{post_id}")
    public ResponseEntity<?> updateComment(@ApiIgnore @AuthenticationPrincipal UserPrincipal currentUser, @Valid @RequestBody CommentDto commentDto, @PathVariable Long post_id, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            throw new BadRequestException(bindingResult.getFieldError().getDefaultMessage());
        }
        if (currentUser.getUsername().equals(commentDto.getUserName()) && commentDto.getId() != null) {
            CommentDto result = commentService.update(currentUser, commentDto, post_id);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId()).toUri();
            return ResponseEntity.created(location).body(result);
        }
        else
            return new ResponseEntity(new ApiResponse(false, "You can edit only your own comments!"), HttpStatus.FORBIDDEN);
    }

    @GetMapping("/comments")
    public List<CommentDto> getAllComment() {
        return commentService.findAll();
    }

    @GetMapping("/comments/{id}")
    public CommentDto getComment(@PathVariable Long id) {
        return commentService.findOne(id);
    }

    @DeleteMapping("/comments/{id}")
    public void deleteComment(@PathVariable Long id, @ApiIgnore @AuthenticationPrincipal UserPrincipal currentUser) {
        commentService.delete(id, currentUser);
    }
}
