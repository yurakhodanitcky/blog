package com.yurakhodanitcky.blog.rest;

import com.yurakhodanitcky.blog.exception.BadRequestException;
import com.yurakhodanitcky.blog.payload.ApiResponse;
import com.yurakhodanitcky.blog.security.UserPrincipal;
import com.yurakhodanitcky.blog.service.PostService;
import com.yurakhodanitcky.blog.service.dto.post.CreatePostRequest;
import com.yurakhodanitcky.blog.service.dto.post.PostDto;
import com.yurakhodanitcky.blog.service.dto.post.PostPageableDto;
import com.yurakhodanitcky.blog.service.dto.post.UpdatePostRequest;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PostResource {
    private final PostService postService;

    public PostResource(PostService postService) {
        this.postService = postService;
    }

    @PostMapping("/posts")
    public ResponseEntity<PostDto> createPost(@ApiIgnore @AuthenticationPrincipal UserPrincipal currentUser,
                                              @Valid @RequestBody CreatePostRequest createPostRequest, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            throw new BadRequestException(bindingResult.getFieldError().getDefaultMessage());
        }
        PostDto result = postService.save(currentUser, createPostRequest);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).body(result);
    }

    @PutMapping("/posts")
    public ResponseEntity<PostDto> updatePost(@ApiIgnore @AuthenticationPrincipal UserPrincipal currentUser,
                                              @Valid @RequestBody UpdatePostRequest updatePostRequest, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            throw new BadRequestException(bindingResult.getFieldError().getDefaultMessage());
        }
        if (currentUser.getUsername().equals(updatePostRequest.getUserName()) && updatePostRequest.getId() != null) {
            PostDto result = postService.update(updatePostRequest);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId()).toUri();
            return ResponseEntity.created(location).body(result);
        }
        else
            return new ResponseEntity(new ApiResponse(false, "You can edit only your own posts!"), HttpStatus.FORBIDDEN);
    }

    @GetMapping("/posts/page")
    public Page<PostPageableDto> getPosts(@RequestParam(value = "page", defaultValue = "0") int page,
                                          @RequestParam(value = "size", defaultValue = "10") int size) {
        return postService.findPage(page, size);
    }

    @GetMapping("/posts")
    public List<PostDto> getAllPost() {
        return postService.findAll();
    }

    @GetMapping("/posts/{id}")
    public PostDto getPost(@PathVariable Long id) {
        return postService.findOne(id);
    }

    @GetMapping("/posts/tag/{tag}")
    public List<PostDto> findALLbyTag(@PathVariable String tag) {
        return postService.findAllByTag(tag);
    }

    @GetMapping("/posts/user/{username}")
    public List<PostDto> findAllByUser(@PathVariable String username) {
        return postService.findAllByUser(username);
    }

    @DeleteMapping("/posts/{id}")
    public void deletePost(@PathVariable Long id, @ApiIgnore @AuthenticationPrincipal UserPrincipal currentUser) {
        postService.delete(id, currentUser);
    }
}