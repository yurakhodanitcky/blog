package com.yurakhodanitcky.blog.repository;

import com.yurakhodanitcky.blog.domain.Post;
import com.yurakhodanitcky.blog.domain.Tag;
import com.yurakhodanitcky.blog.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
    List<Post> findAllByTags(Tag tag);
    List<Post> findAllByUser(User user);
    List<Post> findAllByOrderByCreatedAtDesc();

    @Query(nativeQuery = true, value = "SELECT id, title, LEFT (post_text, 100) post_text, created_at, updated_at, user_id FROM post")
    Page<Post> findPageable(Pageable pageable);

    @Query(value = "SELECT  p.title, p.createdAt, SUBSTRING(p.postText, 1, 100) FROM Post p order by p.createdAt DESC")
    Page<Post> findPageableHQL(Pageable pageable);
}
