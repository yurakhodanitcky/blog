package com.yurakhodanitcky.blog.repository;

import com.yurakhodanitcky.blog.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag, String> {
    boolean existsByName(String name);

    @Override
    <S extends Tag> List<S> saveAll(Iterable<S> entities);
}
