package com.yurakhodanitcky.blog.repository;

import com.yurakhodanitcky.blog.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, String> {
}
