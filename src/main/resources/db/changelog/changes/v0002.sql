insert into roles(name) values ('ROLE_ADMIN');
insert into roles(name) values ('ROLE_USER');
insert into blog_user(id, username, password, first_name, last_name, email)
    values
      (1, 'admin', '$2a$10$TsqaAi9Dn7Z4tOdH9TnuVOHWyAZnJAOAzQcDq5dMzssAsNQdJ/DM2', 'admin', 'admin', 'admin@gmail.com'),
      (2, 'user', '$2a$10$dmuvTDL/Y5ala.k7JZHC.u5JnVOUPR9.CafaSjmeAWP.EjYjVGKiK', 'user', 'user', 'user@gmail.com');
insert into user_roles(user_id, name)
    values
      (1, 'ROLE_ADMIN'),
      (1, 'ROLE_USER'),
      (2, 'ROLE_USER');
